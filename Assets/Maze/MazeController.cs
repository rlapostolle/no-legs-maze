﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MazeGenerator;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MazeController : MonoBehaviour
{

    public GameObject wall;
    public GameObject floor;
    public Text WonText;
    public PlayerController player;

    public MazeInfo Info { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        MazeHandler maze = new MazeHandler(20,40);
        maze.Generate();

        Info = maze.MazeInformations;
        Camera.main.transform.position = new Vector3(maze.MazeInformations.Width / 2, maze.MazeInformations.Height / 2, Camera.main.transform.position.z);
        Camera.main.orthographicSize = maze.MazeInformations.Width / 3;
        MazePrinter maze_printer = new MazePrinter(maze.MazeInformations);
        maze_printer.Print(this.gameObject, wall, floor);

        player.maze = this;
        player.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
        }

        //Check if on exit
        if (((int)player.transform.position.y) == Info.ExitPositionHeight && ((int)player.transform.position.x) == Info.ExitPositionWidth)
        {
            GameObject.FindObjectOfType<Timer>().enabled = false;
            player.enabled = false;
            WonText.enabled = true;
        }
    }
}
