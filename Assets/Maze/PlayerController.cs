﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public Text[] PlayerTexts;

    enum MoveType { Up = 0, Down = 1, Left = 2, Right = 3};
    KeyCode[] MoveCode { get; set; }

    private static readonly char[] MOVE_CHAR = new char[4] { '↑', '↓', '←', '→' };

    public MazeController maze { get; set; }

    const float SHUFFLE_INPUT_DELAY_SEC = 5f;
    float time_left_to_randomize_inputs = SHUFFLE_INPUT_DELAY_SEC;

    /** 
     * Randomize inputs, each direction will be reaffected to an input
     */
    private void RandomizeInputs()
    {
        MoveCode = new KeyCode[4];
        //Randomize inputs
        var player_left = new List<int>() { 0, 1, 2, 3 };
        KeyCode[] available_key_codes;
        if (Config.keyboard_mode)
            available_key_codes = new KeyCode[4] { KeyCode.Quote, KeyCode.W, KeyCode.N, KeyCode.P };
        else
            available_key_codes = new KeyCode[4] { KeyCode.Joystick1Button0, KeyCode.Joystick2Button0, KeyCode.Joystick3Button0, KeyCode.Joystick4Button0 };
        int cur_move_code_index = 0;
        while (player_left.Count > 0)
        {
            var index = Random.Range(0, player_left.Count);
            int player_index = player_left[index];
            MoveCode[cur_move_code_index] = available_key_codes[player_index];
            PlayerTexts[player_index].text = MOVE_CHAR[cur_move_code_index].ToString();

            player_left.RemoveAt(index);
            cur_move_code_index++;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if(Config.easy_mode)
        {
            if(Config.keyboard_mode)
            {
                //use for debug or test purpose, keep simple
                MoveCode = new KeyCode[4] { KeyCode.UpArrow, KeyCode.DownArrow, KeyCode.LeftArrow, KeyCode.RightArrow };
            }
            else
            {
                MoveCode = new KeyCode[4] { KeyCode.Joystick1Button0, KeyCode.Joystick2Button0, KeyCode.Joystick3Button0, KeyCode.Joystick4Button0 };
            }
        }
        else
        {
            RandomizeInputs();
        }

    }

    void Awake()
    {
        //Put the player on the entry point when awake
        this.transform.position = new Vector3(maze.Info.EntryPositionWidth, maze.Info.EntryPositionHeight, this.transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        time_left_to_randomize_inputs -= Time.deltaTime;
        if(time_left_to_randomize_inputs < 0f)
        {
            time_left_to_randomize_inputs = SHUFFLE_INPUT_DELAY_SEC;
            RandomizeInputs();
        }

        if(Input.GetKeyDown(MoveCode[(int)MoveType.Down]))
        {
            int new_y = (int)this.transform.position.y - 1;
            if (new_y >= 0 && maze.Info.Map[new_y, (int)this.transform.position.x] == MazeGenerator.SquareType.Free)
                this.transform.Translate(Vector3.down);
        }
        if(Input.GetKeyDown(MoveCode[(int)MoveType.Up]))
        {
            int new_y = (int)this.transform.position.y + 1;
            if (new_y < maze.Info.Height && maze.Info.Map[new_y, (int)this.transform.position.x] == MazeGenerator.SquareType.Free)
                this.transform.Translate(Vector3.up);
        }
        if (Input.GetKeyDown(MoveCode[(int)MoveType.Left]))
        {
            int new_x = (int)this.transform.position.x - 1;
            if (new_x >= 0 && maze.Info.Map[(int)this.transform.position.y, new_x] == MazeGenerator.SquareType.Free)
                this.transform.Translate(Vector3.left);
        }
        if (Input.GetKeyDown(MoveCode[(int)MoveType.Right]))
        {
            int new_x = (int)this.transform.position.x + 1;
            if (new_x < maze.Info.Width && maze.Info.Map[(int)this.transform.position.y, new_x] == MazeGenerator.SquareType.Free)
                this.transform.Translate(Vector3.right);
        }

    }

}
