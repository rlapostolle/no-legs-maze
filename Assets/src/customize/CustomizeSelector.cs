﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CustomizeSelector : MonoBehaviour
{
    public GameObject Cursor;
    public GameObject SelectionBorder;

    public GameObject ShuffleInputsTimingMode;
    public GameObject EasyMode;
    public GameObject KeyboardMode;

    private const int ModeGridWidth = 3;
    private const int ModeGridHeight = 1;
    private GameObject[,] ModeGrid;
    private int CursorPosX = 0;
    private int CursorPosY = 0;

    private List<GameObject> BorderList;

    void CreateSelectionBorder(GameObject parent)
    {
        var border = Instantiate(SelectionBorder);
        border.transform.position = parent.transform.position;
        border.transform.parent = parent.transform.parent;
        BorderList.Add(border);
    }

    void CleanSelectionBorders()
    {
        foreach(var border in BorderList)
        {
            Destroy(border);
        }
        BorderList.Clear();
    }

    void RefreshCursor()
    {
        Cursor.transform.position = ModeGrid[CursorPosY, CursorPosX].transform.position + (Vector3.up * 85f);
    }

    void RefreshMode()
    {
        CleanSelectionBorders();
        if (Config.shuffle_inputs_timing_mode)
        {
            CreateSelectionBorder(ShuffleInputsTimingMode);
        }
        if (Config.keyboard_mode)
        {
            CreateSelectionBorder(KeyboardMode);
        }
        if (Config.easy_mode)
        {
            CreateSelectionBorder(EasyMode);
        }
    }

    void ToggleMode(GameObject mode)
    {
        if(mode == ShuffleInputsTimingMode && !Config.shuffle_inputs_timing_mode)
        {
            Config.shuffle_inputs_timing_mode = true;
            Config.easy_mode = false;
        }
        if(mode == EasyMode && !Config.easy_mode)
        {
            Config.easy_mode = true;
            Config.shuffle_inputs_timing_mode = false;
        }
        if(mode == KeyboardMode)
        {
            Config.keyboard_mode = !Config.keyboard_mode;
        }

        RefreshMode();
    }

    // Start is called before the first frame update
    void Start()
    {
        BorderList = new List<GameObject>();
        ModeGrid = new GameObject[ModeGridHeight, ModeGridWidth];
        ModeGrid[0, 0] = ShuffleInputsTimingMode;
        ModeGrid[0, 1] = EasyMode;
        ModeGrid[0, 2] = KeyboardMode;

        RefreshCursor();

        RefreshMode();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            SceneManager.LoadScene("MainMenu");

        if(Input.GetKeyDown(KeyCode.LeftArrow))
        {
            CursorPosX--;
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            CursorPosX++;
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            CursorPosY--;
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            CursorPosY++;
        }
        if (CursorPosX < 0)
            CursorPosX = ModeGridWidth - 1;
        if (CursorPosX >= ModeGridWidth)
            CursorPosX = 0;
        if (CursorPosY < 0)
            CursorPosY = ModeGridHeight - 1;
        if (CursorPosY >= ModeGridHeight)
            CursorPosY = 0;

        if (Input.GetKeyDown(KeyCode.Return))
            ToggleMode(ModeGrid[CursorPosY, CursorPosX]);

        RefreshCursor();
    }
}
