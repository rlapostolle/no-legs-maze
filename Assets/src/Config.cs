﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Config
{
    public static bool shuffle_inputs_timing_mode = true;
    public static bool easy_mode = false;
    public static bool keyboard_mode = true;
}
