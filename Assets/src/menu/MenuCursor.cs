﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuCursor : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject play_text;
    public GameObject customize_text;

    GameObject[] choices;
    int cur_index = 0;

    void moveCursor(int index)
    {
        cur_index = index % choices.Length;
        cur_index = cur_index < 0 ? cur_index + choices.Length : cur_index;
        this.transform.position = new Vector3(this.transform.position.x, choices[cur_index].transform.position.y, this.transform.position.z);
    }

    void Start()
    {
        choices = new GameObject[] { play_text, customize_text };
        moveCursor(0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            switch(cur_index)
            {
                case 0:
                    SceneManager.LoadScene("Maze", LoadSceneMode.Single);
                    break;
                case 1:
                    SceneManager.LoadScene("CustomizeMenu", LoadSceneMode.Single);
                    break;
            }
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            moveCursor(cur_index + 1);
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            moveCursor(cur_index - 1);
        }
    }
}
