﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    float timer = 0f;

    CultureInfo Provider { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        Provider = new CultureInfo("en");
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        this.GetComponent<Text>().text = string.Format(Provider, "{0:00}:{1:00.000}", timer/60, timer%60);
    }
}
