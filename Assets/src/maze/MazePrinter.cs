﻿using System;
using UnityEngine;

namespace MazeGenerator
{
    public class MazePrinter
    {
        public MazeInfo MazeInformations { get; set; }

        public MazePrinter(MazeInfo infos)
        {
            this.MazeInformations = infos;
        }

        public void Print(GameObject parent, GameObject wall, GameObject floor)
        {

            for (int i = 0; i < MazeInformations.Height; i++)
            {
                for (int j = 0; j < MazeInformations.Width; j++)
                {
                    GameObject part = null;

                    switch(MazeInformations.Map[i, j])
                    {
                        case SquareType.Wall:
                            part = MonoBehaviour.Instantiate(wall);
                            break;
                        case SquareType.Free:
                            part = MonoBehaviour.Instantiate(floor);
                            break;
                        default:
                            break;
                    }
                    if (part == null)
                        continue;
                    part.transform.parent = parent.transform;
                    part.transform.position = new Vector3(1f*j, 1f*i, 0.0f);
                    if (MazeInformations.EntryPositionHeight == i && MazeInformations.EntryPositionWidth == j)
                        part.GetComponent<SpriteRenderer>().color = Color.gray;
                    if (MazeInformations.ExitPositionHeight == i && MazeInformations.ExitPositionWidth == j)
                        part.GetComponent<SpriteRenderer>().color = Color.yellow;
                }
            }
        }
    }
}
